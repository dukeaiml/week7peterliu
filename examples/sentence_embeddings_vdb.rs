use rust_bert::pipelines::sentence_embeddings::{SentenceEmbeddingsBuilder, SentenceEmbeddingsModelType};
use qdrant_client::client::QdrantClient;
use qdrant_client::qdrant::{
    vectors_config::Config, VectorParams, VectorsConfig, CreateCollection, Distance,
    PointStruct, SearchPoints
};
use serde_json::json;
use std::io;
use std::convert::TryInto;
use std::cmp;
use qdrant_client::qdrant::point_id::PointIdOptions;

fn main() -> anyhow::Result<()> {
    // Create new Tokio runtime
    let rt = tokio::runtime::Runtime::new()?;

    // Initialize Qdrant client
    let client = QdrantClient::from_url("http://localhost:6334").build()?;
  

    // Create a new collection using the async method, but block on it synchronously
    // Attempt to delete the collection if it already exists (ignore error if it does not exist)
    let _ = rt.block_on(client.delete_collection("sentence_embeddings"));

    // Then create the collection anew
    let create_collection_response = rt.block_on(client.create_collection(&CreateCollection {
        collection_name: "sentence_embeddings".to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 384,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }))?;

    println!("Collection creation response: {:?}", create_collection_response);

    // Set-up sentence embeddings model
    let model = SentenceEmbeddingsBuilder::remote(SentenceEmbeddingsModelType::AllMiniLmL12V2)
        .create_model()?;

    let mut sentences = Vec::new();
    println!("Enter sentences (type 'END' to stop):");
    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input)?;
        let trimmed = input.trim().to_string();
        if trimmed == "END" {
            break;
        }
        sentences.push(trimmed);
    }

    // Generate and store embeddings
    let embeddings = model.encode(&sentences)?;
    for (i, embedding) in embeddings.iter().enumerate() {
        let point = PointStruct::new(
            i as u64,
            embedding.to_vec(),
            json!({"sentence": sentences[i]}).try_into().expect("Failed to convert payload")
        );
        rt.block_on(client.upsert_points("sentence_embeddings".to_string(), None, vec![point], None))?;
    }
    println!("Sentences and their embeddings have been stored.");

    println!("Enter a query sentence:");
    let mut query_sentence = String::new();
    io::stdin().read_line(&mut query_sentence)?;

    let query_embeddings = model.encode(&[query_sentence.trim()])?;
    let search_result = rt.block_on(client.search_points(&SearchPoints {
        collection_name: "sentence_embeddings".to_string(),
        vector: query_embeddings[0].to_vec(),
        limit: 3,
        with_payload: Some(true.into()),
        ..Default::default()
    }))?;
    println!("Search results based on the query: {:?}", search_result);

    // Visualization of the results
    println!("Visualizing the search results:");
    
    let max_bar_width = 50; // Maximum width of the bar in characters
    // Fixing max_score calculation - keep everything as f32
    let max_score = search_result.result.iter().map(|sp| sp.score).fold(0.0f32, f32::max);

  
    for scored_point in search_result.result.iter() {
        // Ensure types align (f32 only), no division by f64 or multiplication by f64
        let bar_length = ((scored_point.score / max_score) * max_bar_width as f32).round() as usize;
        let bar = "█".repeat(cmp::max(bar_length, 1));
    
        let sentence_index = match &scored_point.id {
            Some(point_id) => point_id.point_id_options.as_ref().map(|options| {
                match options {
                    PointIdOptions::Num(num) => *num as usize,
                    _ => usize::MAX // Use an invalid index if there's no direct number, adjust as necessary
                }
            }).unwrap_or(usize::MAX),
            None => usize::MAX // No ID available
        };
    
        // Create an "Unknown sentence" String directly
        let unknown_sentence = "Unknown sentence".to_string(); // Change to String type
        let sentence = sentences.get(sentence_index).unwrap_or(&unknown_sentence); // This now matches &String
    
        println!("{: <20}: {: >3} [{}]", sentence, scored_point.score, bar);
    }
    




    Ok(())
}
