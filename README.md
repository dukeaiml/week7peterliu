# Store User-Entered Sentence Embeddings Into a Qdrant Vector Database All in Rust.


## Table of Contents
1. [Deliverables](#deliverables)
2. [Description](#description)
3. [Installation and Usage](#Installation and Usage)
4. [Development Process](#development-process)

## Deliverables
Youtube video with demonstration:

https://youtu.be/Rk6rPDRApJY


## Description

This is a service which allows the user to type as sentences to be converted into sentence embeddings stored, which are then stored in a Qdrant vector database. Then the user can query the vector database to extract the semantic similarity between entries in the database and the query. This is visualized using a bar graph. This project was made using rust-bert and Qdrant vector database, and is entirely Rust-based. 

Check the file examples/sentence_embeddings_vdb.rs to see my main code.

## Installation and Usage

To run this project locally, ensure Rust, Cargo, are installed on your system. Follow these installation steps:

1. Clone the repository:


git clone git@gitlab.com:dukeaiml/week7peterliu.git

2. Make sure docker is installed.
Do 

docker pull qdrant/qdrant

Then do 

docker run -p 6333:6333 -p 6334:6334 \
    -v $(pwd)/qdrant_storage:/qdrant/storage:z \
    qdrant/qdrant

3. Then install the packages required for rust-bert. 

For Linux:

export LIBTORCH=/path/to/libtorch
export LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH

For Windows:

$Env:LIBTORCH = "X:\path\to\libtorch"
$Env:Path += ";X:\path\to\libtorch\lib"

For MacOS/Homebrew:

brew install pytorch jq
export LIBTORCH=$(brew --cellar pytorch)/$(brew info --json pytorch | jq -r '.[0].installed[0].version')
export LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH

4. Invoke the function with

cargo run --example sentence_embeddings_vdb

## Development Process

Development steps included:

1. Environment Setup: Install the necessary dependencies for Qdrant and rust-bert.
2. Implementation: Write code to ask users for sentences and embed them using rust-bert functions. Storing them into a newly made Qdrant collection.
3. Implementation: Taking a user query and finding similarities using Qdrant SearchPoints. Displaying the results in a custom bar graph function.
3. Testing: Ensuring functionality and handling errors.
4. Documentation: Documenting code and writing this README.


